#!/usr/bin/env python3

from http.server import BaseHTTPRequestHandler, HTTPServer
import logging
import urllib.parse
from datetime import datetime
from planeta import PlanetaKino

from urllib.parse import urlparse, parse_qs


class S(BaseHTTPRequestHandler):
    pk = PlanetaKino()

    def _set_response(self, code=200):
        self.send_response(code)
        self.send_header('Content-type', 'text/json')
        self.end_headers()

    def do_GET(self):
        logging.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))

        if self.path:
            parsed = urlparse(self.path)
            path = parsed.path[1:].lower()
            #            print(parsed)
            params = parse_qs(parsed.query, keep_blank_values=True)
            logging.info(" Parameters: " + str(params))
            id = ''.join(params["id"]) if "id" in params else None
            start_date = ''.join(params["startDate"]) if "startDate" in params else None
            end_date = ''.join(params["endDate"]) if "endDate" in params else None

            if path == "today":
                logging.info(" GET \'today\' command...")

                if start_date is not None and end_date is not None:
                    response = self.pk.get_schedule_by_date(start_date, end_date)
                else:
                    response = self.pk.get_today_schedule()

                logging.info("today command: Success")
                self._set_response(200)
                self.wfile.write(response)

            elif path == "movie":
                logging.info(" GET \'movie\' command...")

                if not id:
                    logging.info(" Command \'%s\' doesn't have mandatory parameters..." % path)
                    self._set_response(404)
                    self.wfile.write(
                        "{\"Error\": \"Command \'movie\' doesn't have mandatory parameters...\"}".encode("utf-8"))

                if start_date and end_date:
                    response = self.pk.get_movie_schedule_by_date(movie_id=id, start_date=start_date,
                                                                  end_date=end_date)
                else:
                    response = self.pk.get_today_movie_schedule(movie_id=id)

                logging.info("movie command: Success")
                self._set_response(200)
                self.wfile.write(response)

            else:
                logging.info(" GET \'%s\' command not found..." % path)
                self._set_response(404)
                self.wfile.write("{\"Error\": \"Command not found\"}".encode("utf-8"))


# def do_POST(self):
#     content_length = int(self.headers['Content-Length'])  # <--- Gets the size of data
#     post_data = self.rfile.read(content_length)  # <--- Gets the data itself
#     logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",
#                  str(self.path), str(self.headers), post_data.decode('utf-8'))
#
#     self._set_response()
#     self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))


def run(server_class=HTTPServer, handler_class=S, port=8080):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')


def run2():
    response = []
    js = {}

    for i in range(0, 5):
        js["id"] = i
        js["link"] = "link_%d" % i
        js["name"] = "name_%d" % i
        response.append(js)

    print(response)
    import json
    print(json.dumps(response))


if __name__ == '__main__':
    from sys import argv

    pk = PlanetaKino()
    # print(pk.get_today_movie_schedule("00000000000000000000000000002895"))
    # print(pk.get_movie_schedule_by_date(movie_id="00000000000000000000000000002895", start_date="2020-12-10",
    #                                    end_date="2021-05-10"))

    # print(pk.get_today_schedule())
    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
