import requests
import json
from datetime import datetime
from dateutil.relativedelta import relativedelta


class PlanetaKino(object):

    def __init__(self):
        current_date = datetime.today()
        self.today = current_date.strftime("%Y-%m-%d")
        self.today_plus_5_month = (current_date + relativedelta(months=5)).strftime("%Y-%m-%d")

    def __get_schedule(self, start_date, end_date) -> str:
        url = "https://planetakino.ua/showtimes/json/?dateStart=%s&dateEnd=%s" % (start_date, end_date)
        response = requests.get(url)
        theater = response.json()

        theater_id = theater["showTimes"]
        for th in theater_id:
            theater_id = th["theaterId"]
            break

        movie_json_list = theater["moviesDetails"]
        response_json = []
        poster_link = "https://planetakino.ua"
        for movies in movie_json_list:
            for movie_id in movies:
                movie_link = "https://planetakino.ua/showtimes/json/?dateStart=%s&dateEnd=%s&movieId=%s" % (
                    start_date, end_date, movie_id)

                movie_json = {"id": movie_id, "link": movie_link, "name": movies[movie_id]["name"],
                              "poster": poster_link + movies[movie_id]["poster"],
                              "bigPoster": poster_link + movies[movie_id]["bigPoster"],
                              "bigPoster360x553": poster_link + movies[movie_id]["bigPoster360x553"],
                              "theaterId": str(theater_id)}
                response_json.append(movie_json)

        return json.dumps(response_json).encode("utf-8")

    def get_schedule_by_date(self, start_date, end_date):
        if start_date and end_date:
            return self.__get_schedule(start_date=start_date, end_date=end_date)

    def get_today_schedule(self):
        return self.__get_schedule(start_date=self.today, end_date=self.today)

    def __get_movie_schedule(self, url) -> str:
        movie_response = requests.get(url)
        movie_json = movie_response.json()["showTimes"]

        response_json = []

        for mv in movie_json:
            datetime_obj = datetime.strptime(mv["timeBegin"], '%Y-%m-%d %H:%M:%S')
            schedule_json = {"id": mv["id"], "date": str(datetime_obj.date()), "time": str(datetime_obj.time()),
                             "technologyId": mv["technologyId"], "format": mv["format"]}
            response_json.append(schedule_json)

        return json.dumps(response_json).encode("utf-8")

    def get_today_movie_schedule(self, movie_id) -> str:
        movie_url = "https://planetakino.ua/showtimes/json/?dateStart=%s&dateEnd=%s&movieId=%s" % (
            self.today, self.today, movie_id)

        return self.__get_movie_schedule(url=movie_url)

    def get_movie_schedule_by_date(self, movie_id, start_date, end_date):
        if start_date and end_date:
            movie_url = "https://planetakino.ua/showtimes/json/?dateStart=%s&dateEnd=%s&movieId=%s" % (
                start_date, end_date, movie_id)
            return self.__get_movie_schedule(movie_url)
