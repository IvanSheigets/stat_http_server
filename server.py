import requests
import json
from datetime import date
from dateutil.relativedelta import relativedelta

moviesList = {}
sessionList = {}

mLink = "https://planetakino.ua/showtimes/json/?dateStart=2020-12-08&dateEnd=2021-05-08&movieId=00000000000000000000000000002927"


def get_today():
    currentDate = date.today()
    startDate = currentDate.strftime("%Y-%m-%d")
    endDate = (currentDate + relativedelta(months=5)).strftime("%Y-%m-%d")
    todayUrl = "https://planetakino.ua/showtimes/json/?dateStart=%s&dateEnd=%s" % (startDate, endDate)
    todayResponse = requests.get(todayUrl)
    movieJsonList = todayResponse.json()["moviesDetails"]

    for movies in movieJsonList:
        for movieID in movies:
            movieLink = "https://planetakino.ua/showtimes/json/?dateStart=%s&dateEnd=%s&movieId=%s" % (
                startDate, endDate, movieID)
            moviesList[movieLink] = movies[movieID]["name"]
            # print(movieID + " - " + movies[movieID]["name"])


def get_movie(url):
    movieResponse = requests.get(url)
    movieJson = movieResponse.json()["showTimes"]

    for movie in movieJson:
        sessionList[movie["id"]] = movie["theaterId"]



def get_hall(sesionId):
    sessionLink = "https://pay.planetakino.ua/api/v1/cart/show-times?showTimeId=%s&theaterId=%s&includeSeats=y&includeDetail=y" % (sesionId, sessionList[sesionId])
    #"https://pay.planetakino.ua/api/v1/cart/halls?showtimeId=761697"
    print(sessionLink)

    # print(movieJson)


if __name__ == "__main__":
    # moviesList["sdsd"] = "sdsd"
    #

    get_today()
    res = list(moviesList.keys())[0]
    print(res + " - " + moviesList[res])
    get_movie(res)
    print(sessionList)
    get_hall(list(sessionList.keys())[0])

    # print (moviesList)
